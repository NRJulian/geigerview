# GeigerView 
GeigerView is a modular, graphical desktop application for visualizing and interfacing with the GMC300 series geiger counter devices available from [GQ Electronics](https://www.gqelectronicsllc.com/). 

![Screenshot](./images/screenshot.png)
*GeigerView measuring a shard of [Fiestaware](https://www.epa.gov/radtown/radioactivity-antiques)*

Features:
 - Modular widget-like window system can display custom gauges, graphs, units and pictoral visualizations of measured radiation levels
 - Can send remote commands to the GMC300 device, including power off and power on
  - Auto-connects to compatible GMC300 devices
 - Cross-platform compatability (runs on Windows, Mac and Linux)

## Legal Notice

This software was written by someone who bought a geiger counter after watching HBO's Chernobyl and too many YouTube videos. This software is intended for demonstration and hobbyist purposes in normal, everyday environments only and should *never* be used in *any* situations where health or safety is at risk in any way. Per the GPLv3 header: `This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.` The software author *can not be held liable for any damages inflicted by this software or the use of this software.*

## Installation

GeigerView is a portable Java desktop application and can be run by simply downloading and double-clicking the most recent JAR file available on the releases page. You will need to have a Java runtime installed on your machine. You can download AdoptOpenJDK [here](https://adoptopenjdk.net/).

**Windows 10 users:** On high resolution displays, Windows 10 zoom settings will distort the graphics in this program (particularly the Visualization Window and main menu icons). If you encounter this, [set the desktop 'Scale and layout' settings](https://www.lifewire.com/using-windows-10-display-scaling-4587328) to 100% when using GeigerView.

## Usage:
1. Make sure you have installed the necessary drivers for your GMC300 device from [GQ Electronics](http://gqelectronicsllc.com/comersus/store/download.asp)
2. Connect your GMC300 device to your computer using the USB cable
3. Run GeigerView, and use the connection panel to connect to your device. You can use the 'Autoconnect' button to try to automatically detect your device. If that doesn't work, you can enter your serial connection details manually and use the 'Connect' button.
![tutorial1](./images/tutorial1.png)
4. Once the device is successfully connected, make sure your GMC 300 device is turned on. You can  use the 'Power On' button on the Command panel to turn it on if needed. Then use the Monitoring panel to choose an update interval (1 second is recommended) and click 'Start' to begin monitoring.
![tutorial2](./images/tutorial2.png)
5. Use the four new window buttons on the Monitoring panel to launch new monitoring windows. These are like widgets and can be opened, resized, edited and moved around as you see fit. Use each window's file menu to customize things like units, colors and range.
![tutorial3](./images/tutorial3.png)
6. You can use the 'Commands' panel to send commands to the device. The 'Click On/Off' button will send a macro script which will toggle the click (note: this is really a preview of a Macro feature and doesn't always work perfectly)
![tutorial4](./images/tutorial4.png)
7. You can use the 'File' menu to save and load window layouts. Right now, the app only saves the window layout and does not save unit, color, etc...
8. When you are ready to stop monitoring, use the 'Stop' button on the Monitoring panel to quit. Use the 'Disconnect' button on the Connection panel to disconnect from the device.

## Licensing:

GeigerView is written entirely in Java Swing and is free (libre) software licensed under the GNU GPL v.3.0 license. GeigerView uses the following open-source libraries:
 - [jSerialComm](https://github.com/Fazecast/jSerialComm) ([GNU GPL v3.0](https://github.com/Fazecast/jSerialComm/blob/master/COPYING))
 - [SteelSeries](https://github.com/HanSolo/SteelSeries-Swing) ([BSD-3-Clause License](https://github.com/HanSolo/SteelSeries-Swing/blob/master/LICENSE.txt))
 - [XChart](https://github.com/knowm/XChart) ([Apache 2.0](https://github.com/knowm/XChart/blob/develop/LICENSE))
 - [Jackson Databind](https://github.com/FasterXML/jackson-databind) ([Apache 2.0](https://github.com/FasterXML/jackson-databind/blob/master/LICENSE))

GeigerView is written using NetBeans and Maven. As such, the end user can replace or upgrade any of these libraries by opening the project in NetBeans, editing the dependency details in the `pom.xml` file, and rebuilding the project. Details can be found here:
 - [Using NetBeans and Maven](http://wiki.netbeans.org/Maven)

## Wishlist/Roadmap:
- Improve auto-sizing of the Units window text
- Add a temperature graph marked with common dosages
- Add ability to make custom visualization/picture windows
- Add ability to create and save macros
- Move from Swing to JavaFX