/*
GeigerView
Copyright (C) 2021 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.geigerview;

import com.fazecast.jSerialComm.SerialPort;
import java.io.IOException;
import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import net.wizarddigital.geigerview.model.GMCReading;

public class GMCClient {

    String comPortName;
    int baudRate;
    boolean isConnected = false;
    SerialPort comPort;

    public GMCClient() {
        //Empty constructor
    }

    public GMCClient(String comPortName, int baudRate) {
        this.comPortName = comPortName;
        this.baudRate = baudRate;
    }

    public String getComPortName() {
        return comPortName;
    }

    public void setComPortName(String comPortName) {
        this.comPortName = comPortName;
    }

    public int getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(int baudRate) {
        this.baudRate = baudRate;
    }

    public void connect() throws ConnectException {
        try {
            comPort = SerialPort.getCommPort(comPortName);
            comPort.setComPortParameters(baudRate, 8, 1, 0);
            comPort.openPort();
            if (!getVersion().contains("GMC-")) {
                throw new ConnectException("COM port opened but unable to verify GMC device: Confirm COM port settings and device connection.");
            }
            isConnected = true;
        } catch (Exception e) {
            throw new ConnectException("Unable to open Serial connection: Please confirm COM port settings and that COM port is not already in use.");
        }
    }

    public void autoConnect() throws ConnectException {
        SerialPort[] commPorts = SerialPort.getCommPorts();
        int[] baudRates = {115200, 57600, 38400, 28800, 19200, 14400, 9600, 4800, 2400, 1200};
        for (SerialPort port : commPorts) {
            if (port.getDescriptivePortName().contains("CH340")) {
                for (int baudRate : baudRates) {
                    try {
                        comPortName = port.getSystemPortName();
                        this.baudRate = baudRate;
                        connect();
                        return;
                    } catch (Exception e) {
                        disconnect();
                    }
                }
            };
        }
        throw new ConnectException("Unable to find GMC device: please ensure it is connected and the drivers are installed");
    }

    public void disconnect() {
        comPort.closePort();
        isConnected = false;
    }

    public boolean testConnection() throws IOException {
        if (getVersion().contains("GMC-")) {
            return true;
        } else {
            return false;
        }
    }

    public String getVersion() throws IOException {
        sendCommand("<GETVER>>");
        byte[] data = receiveData();
        return new String(data);
    }

    public void sendCommand(String command) {
        byte[] b = command.getBytes(StandardCharsets.UTF_8);
        comPort.writeBytes(b, b.length);
    }

    byte[] receiveData() throws IOException {
        try {
            int waitCount = 0;
            while (comPort.bytesAvailable() == 0 && waitCount < 10) {
                Thread.sleep(100);
                waitCount++;
            }
            byte[] buffer = new byte[comPort.bytesAvailable()];
            comPort.readBytes(buffer, buffer.length);
            return buffer;
        } catch (InterruptedException e) {
            throw new IOException("An error occured when trying to receive data from GMC device: " + e.getMessage());
        }
    }

    //NEED TO ADD ERROR HANDLING
    public void powerOn() {
        sendCommand("<POWERON>>");
    }

    public void powerOff() {
        sendCommand("<POWEROFF>>");
    }

    public void reboot() {
        sendCommand("<REBOOT>>");
    }

    public void keyS1() {
        sendCommand("<KEY0>>");
    }

    public void keyS2() {
        sendCommand("<KEY1>>");
    }

    public void keyS3() {
        sendCommand("<KEY2>>");
    }

    public void keyS4() {
        sendCommand("<KEY3>>");
    }

    public GMCReading read() throws IOException, IllegalStateException {
        if (isConnected) {
            sendCommand("<GETCPM>>");
            byte[] data = receiveData();
            short cpmShort = ByteBuffer.wrap(data).getShort();
            int cpmInteger = Short.toUnsignedInt(cpmShort);
            return new GMCReading(cpmInteger);
        } else {
            throw new IllegalStateException("GMC300 device not connected: ensure connection is established before attempting to read data.");
        }
    }
}