/*
GeigerView
Copyright (C) 2021 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.geigerview.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import net.wizarddigital.geigerview.model.ImageCoordinates;

/**
 *
 * @author Nick
 */
public class ImagePanel extends JPanel {

    private BufferedImage image;
    private int currentCount = 0;
    private int previousCount = 0;
    private Dimension currentSize;
    private Dimension previousSize;
    ArrayList<ImageCoordinates> imageCoordinates = new ArrayList<>();
    
    public ImagePanel() {
        //Empty constructor
    }

    public void setImage(String filePath){
        try {
            image = ImageIO.read(getClass().getResource(filePath));
        } catch (IOException ex) {
            //System.out.println(ex.getMessage());
        }
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.currentSize = this.getSize();
        if (!this.getSize().equals(this.previousSize)){
            this.previousSize = this.getSize();
            resetImageLocations();
        }
        for (ImageCoordinates imageCoordinate : imageCoordinates) {
            g.drawImage(image, imageCoordinate.getX(), imageCoordinate.getY(), this);  
        }
    }

    public void setCount(int number) {
        this.previousCount = currentCount;
        this.currentCount = number;
        if (currentCount < previousCount) {
            removeImages(previousCount-currentCount);
        } else if (currentCount > previousCount){
            addImages(currentCount-previousCount);
        }
        this.repaint();
    }

    public void addImages(int number) {
        for (int i = 0; i < number; i++) {
            Random random = new Random();
            int x = random.nextInt(this.getWidth() - image.getWidth());
            int y = random.nextInt(this.getHeight() - image.getHeight());
            imageCoordinates.add(new ImageCoordinates(x, y));
        }

    }

    public void removeImages(int number) {
        for (int i = 0; i < number; i++) {
            imageCoordinates.remove(imageCoordinates.size() - 1);
        }
    }

    public void clearImages(){
        imageCoordinates = new ArrayList<>();
        this.repaint();
    }
    
    public void resetImageLocations(){
        for (ImageCoordinates coordinates : imageCoordinates) {
            Random random = new Random();
            int x = random.nextInt(this.getWidth() - image.getWidth());
            int y = random.nextInt(this.getHeight() - image.getHeight());
            coordinates.setPosition(x, y);
        }
    }
}
