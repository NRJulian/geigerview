/*
GeigerView
Copyright (C) 2021 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.geigerview.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import net.wizarddigital.geigerview.model.GMCReading;
import net.wizarddigital.geigerview.model.WindowSpecifications;
import net.wizarddigital.geigerview.model.WindowSpecifications.WindowType;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;

public class XChartForm extends javax.swing.JFrame implements ReadingForm {

    XYChart chart;
    XChartPanel panel;
    ArrayList<Date> xData = new ArrayList<>();
    ArrayList<Double> yData = new ArrayList<>();
    RadiationUnit radiationUnit = RadiationUnit.CPM;
    int maxColumns = 25;
    boolean isPaused = false;
    WindowType windowType;

    public enum Theme {
        DEFAULT,
        URANIUM,
        CHERENKOV,
        RADIUM,
        FALLOUT,
        MONO_1,
        MONO_2
    }
    Theme theme;

    public XChartForm() {
        initComponents();
        windowType = WindowType.GRAPH;
        initGraph();
        this.setIconImage(new ImageIcon(getClass().getResource("logo.png")).getImage());
        this.setTitle("Radiation Graph");
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public XChartForm(Point location, Dimension dimension, Map settings) {
        initComponents();
        windowType = WindowType.GRAPH;
        initGraph();
        this.setIconImage(new ImageIcon(getClass().getResource("logo.png")).getImage());
        this.setTitle("Radiation Graph");
        this.setLocation(location);
        this.setSize(dimension);
        loadSettings(settings);
        this.setVisible(true);
    }

    void initGraph() {
        xData.add(new Date());
        yData.add(new Double(0));
        chart = new XYChartBuilder().width(800).height(600).title("Counts per Minute").build();
        chart.addSeries("series", xData, yData);
        xData.remove(0);
        yData.remove(0);
        chart.getStyler().setLegendVisible(false);
        chart.getStyler().setXAxisLabelRotation(90);
        chart.getStyler().setZoomEnabled(true);
        panel = new XChartPanel(chart);
        this.theme = Theme.DEFAULT;
        this.setContentPane(panel);
    }

    void setChartColors(Color backgroundColor, Color[] seriesColor, Color gridLineColor, Color borderColor) {
        chart.getStyler().setPlotBackgroundColor(backgroundColor);
        chart.getStyler().setSeriesColors(seriesColor);
        chart.getStyler().setPlotGridLinesColor(gridLineColor);
        chart.getStyler().setPlotBorderColor(borderColor);
        chart.removeSeries("series");
        if (xData.size() != 0) {
            chart.addSeries("series", xData, yData);
        } else {
            xData.add(new Date());
            yData.add(new Double(0));
            chart.addSeries("series", xData, yData);
            xData.remove(0);
            yData.remove(0);
        }
        panel.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuBar1 = new javax.swing.JMenuBar();
        unitMenu = new javax.swing.JMenu();
        unitMenuItemCPM = new javax.swing.JMenuItem();
        unitMenuItemCPS = new javax.swing.JMenuItem();
        unitMenuItemMicrosievert = new javax.swing.JMenuItem();
        unitMenuItemMillisievert = new javax.swing.JMenuItem();
        unitMenuItemMicroRem = new javax.swing.JMenuItem();
        unitMenuItemMilliRen = new javax.swing.JMenuItem();
        unitMenuItemMicroRoentgen = new javax.swing.JMenuItem();
        unitMenuItemMilliRoentgen = new javax.swing.JMenuItem();
        unitMenuItemBananaEquivalentDose = new javax.swing.JMenuItem();
        columnsMenu = new javax.swing.JMenu();
        columnsMenuItem5 = new javax.swing.JMenuItem();
        columnsMenuItem10 = new javax.swing.JMenuItem();
        columnsMenuItem25 = new javax.swing.JMenuItem();
        columnsMenuItem100 = new javax.swing.JMenuItem();
        columnsMenuItem500 = new javax.swing.JMenuItem();
        columnsMenuItem1000 = new javax.swing.JMenuItem();
        columnsMenuItem10000 = new javax.swing.JMenuItem();
        themeMenu = new javax.swing.JMenu();
        themeMenuItemDefault = new javax.swing.JMenuItem();
        themeMenuItemUranium = new javax.swing.JMenuItem();
        themeMenuItemCherenkov = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        themeMenuItemFallout = new javax.swing.JMenuItem();
        themeMenuItemMono1 = new javax.swing.JMenuItem();
        themeMenuItemMono2 = new javax.swing.JMenuItem();
        pauseMenu = new javax.swing.JMenu();
        pauseMenuItemPause = new javax.swing.JMenuItem();

        jMenuItem6.setText("jMenuItem6");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        unitMenu.setText("Unit");

        unitMenuItemCPM.setText("CPM");
        unitMenuItemCPM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemCPMActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemCPM);

        unitMenuItemCPS.setText("CPS");
        unitMenuItemCPS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemCPSActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemCPS);

        unitMenuItemMicrosievert.setText("uSv/hr");
        unitMenuItemMicrosievert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemMicrosievertActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemMicrosievert);

        unitMenuItemMillisievert.setText("mSv/hr");
        unitMenuItemMillisievert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemMillisievertActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemMillisievert);

        unitMenuItemMicroRem.setText("uRem/hr");
        unitMenuItemMicroRem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemMicroRemActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemMicroRem);

        unitMenuItemMilliRen.setText("mRem/hr");
        unitMenuItemMilliRen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemMilliRenActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemMilliRen);

        unitMenuItemMicroRoentgen.setText("uRoentgen/hr");
        unitMenuItemMicroRoentgen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemMicroRoentgenActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemMicroRoentgen);

        unitMenuItemMilliRoentgen.setText("mRoentgen/hr");
        unitMenuItemMilliRoentgen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemMilliRoentgenActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemMilliRoentgen);

        unitMenuItemBananaEquivalentDose.setText("Banana Eq. Dose");
        unitMenuItemBananaEquivalentDose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unitMenuItemBananaEquivalentDoseActionPerformed(evt);
            }
        });
        unitMenu.add(unitMenuItemBananaEquivalentDose);

        jMenuBar1.add(unitMenu);

        columnsMenu.setText("Columns");

        columnsMenuItem5.setText("5");
        columnsMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsMenuItem5ActionPerformed(evt);
            }
        });
        columnsMenu.add(columnsMenuItem5);

        columnsMenuItem10.setText("10");
        columnsMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsMenuItem10ActionPerformed(evt);
            }
        });
        columnsMenu.add(columnsMenuItem10);

        columnsMenuItem25.setText("25");
        columnsMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsMenuItem25ActionPerformed(evt);
            }
        });
        columnsMenu.add(columnsMenuItem25);

        columnsMenuItem100.setText("100");
        columnsMenuItem100.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsMenuItem100ActionPerformed(evt);
            }
        });
        columnsMenu.add(columnsMenuItem100);

        columnsMenuItem500.setText("500");
        columnsMenuItem500.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsMenuItem500ActionPerformed(evt);
            }
        });
        columnsMenu.add(columnsMenuItem500);

        columnsMenuItem1000.setText("1000");
        columnsMenuItem1000.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsMenuItem1000ActionPerformed(evt);
            }
        });
        columnsMenu.add(columnsMenuItem1000);

        columnsMenuItem10000.setText("10000");
        columnsMenuItem10000.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                columnsMenuItem10000ActionPerformed(evt);
            }
        });
        columnsMenu.add(columnsMenuItem10000);

        jMenuBar1.add(columnsMenu);

        themeMenu.setText("Theme");

        themeMenuItemDefault.setText("Default");
        themeMenuItemDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                themeMenuItemDefaultActionPerformed(evt);
            }
        });
        themeMenu.add(themeMenuItemDefault);

        themeMenuItemUranium.setText("Uranium");
        themeMenuItemUranium.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                themeMenuItemUraniumActionPerformed(evt);
            }
        });
        themeMenu.add(themeMenuItemUranium);

        themeMenuItemCherenkov.setText("Cherenkov");
        themeMenuItemCherenkov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                themeMenuItemCherenkovActionPerformed(evt);
            }
        });
        themeMenu.add(themeMenuItemCherenkov);

        jMenuItem2.setText("Radium");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        themeMenu.add(jMenuItem2);

        themeMenuItemFallout.setText("Fallout");
        themeMenuItemFallout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                themeMenuItemFalloutActionPerformed(evt);
            }
        });
        themeMenu.add(themeMenuItemFallout);

        themeMenuItemMono1.setText("Mono 1");
        themeMenuItemMono1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                themeMenuItemMono1ActionPerformed(evt);
            }
        });
        themeMenu.add(themeMenuItemMono1);

        themeMenuItemMono2.setText("Mono 2");
        themeMenuItemMono2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                themeMenuItemMono2ActionPerformed(evt);
            }
        });
        themeMenu.add(themeMenuItemMono2);

        jMenuBar1.add(themeMenu);

        pauseMenu.setText("Pause");

        pauseMenuItemPause.setText("Pause");
        pauseMenuItemPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pauseMenuItemPauseActionPerformed(evt);
            }
        });
        pauseMenu.add(pauseMenuItemPause);

        jMenuBar1.add(pauseMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 274, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void themeMenuItemUraniumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_themeMenuItemUraniumActionPerformed
        this.theme = Theme.URANIUM;
        setChartColors(Color.LIGHT_GRAY, new Color[]{Color.DARK_GRAY}, Color.BLACK, Color.BLACK);

    }//GEN-LAST:event_themeMenuItemUraniumActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        setChartColors(Color.BLACK, new Color[]{Color.GREEN}, Color.DARK_GRAY, Color.GREEN);
        this.theme = Theme.RADIUM;
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void themeMenuItemFalloutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_themeMenuItemFalloutActionPerformed
        setChartColors(Color.BLUE, new Color[]{Color.YELLOW}, Color.DARK_GRAY, Color.WHITE);
        this.theme = Theme.FALLOUT;
    }//GEN-LAST:event_themeMenuItemFalloutActionPerformed

    private void themeMenuItemMono2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_themeMenuItemMono2ActionPerformed
        setChartColors(Color.BLACK, new Color[]{Color.WHITE}, Color.WHITE, Color.BLACK);
        this.theme = Theme.MONO_2;
    }//GEN-LAST:event_themeMenuItemMono2ActionPerformed

    private void themeMenuItemMono1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_themeMenuItemMono1ActionPerformed
        setChartColors(Color.WHITE, new Color[]{Color.BLACK}, Color.BLACK, Color.WHITE);
        this.theme = Theme.MONO_1;
    }//GEN-LAST:event_themeMenuItemMono1ActionPerformed

    private void themeMenuItemCherenkovActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_themeMenuItemCherenkovActionPerformed
        setChartColors(Color.BLUE, new Color[]{Color.CYAN}, Color.DARK_GRAY, Color.BLACK);
        this.theme = Theme.CHERENKOV;
    }//GEN-LAST:event_themeMenuItemCherenkovActionPerformed

    private void columnsMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_columnsMenuItem5ActionPerformed
        maxColumns = 5;
    }//GEN-LAST:event_columnsMenuItem5ActionPerformed

    private void columnsMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_columnsMenuItem10ActionPerformed
        maxColumns = 10;
    }//GEN-LAST:event_columnsMenuItem10ActionPerformed

    private void columnsMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_columnsMenuItem25ActionPerformed
        maxColumns = 25;
    }//GEN-LAST:event_columnsMenuItem25ActionPerformed

    private void columnsMenuItem100ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_columnsMenuItem100ActionPerformed
        maxColumns = 100;
    }//GEN-LAST:event_columnsMenuItem100ActionPerformed

    private void columnsMenuItem500ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_columnsMenuItem500ActionPerformed
        maxColumns = 500;
    }//GEN-LAST:event_columnsMenuItem500ActionPerformed

    private void columnsMenuItem1000ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_columnsMenuItem1000ActionPerformed
        maxColumns = 1000;
    }//GEN-LAST:event_columnsMenuItem1000ActionPerformed

    private void columnsMenuItem10000ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_columnsMenuItem10000ActionPerformed
        maxColumns = 10000;
    }//GEN-LAST:event_columnsMenuItem10000ActionPerformed

    private void themeMenuItemDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_themeMenuItemDefaultActionPerformed
        setChartColors(Color.WHITE, new Color[]{Color.BLUE}, Color.LIGHT_GRAY, Color.GRAY);
        this.theme = Theme.DEFAULT;
    }//GEN-LAST:event_themeMenuItemDefaultActionPerformed

    private void pauseMenuItemPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pauseMenuItemPauseActionPerformed
        if (isPaused) {
            isPaused = false;
            pauseMenuItemPause.setText("Pause");
            reset();
        } else {
            isPaused = true;
            pauseMenuItemPause.setText("Restart");
        }
    }//GEN-LAST:event_pauseMenuItemPauseActionPerformed

    private void unitMenuItemMilliRoentgenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemMilliRoentgenActionPerformed
        configureChart("mRoentgen Per Hour", RadiationUnit.MICROROENTGEN, "0.000");
    }//GEN-LAST:event_unitMenuItemMilliRoentgenActionPerformed

    private void unitMenuItemMilliRenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemMilliRenActionPerformed
        configureChart("mRem Per Hour", RadiationUnit.MILLIREM, "0.00");
    }//GEN-LAST:event_unitMenuItemMilliRenActionPerformed

    private void unitMenuItemMicroRemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemMicroRemActionPerformed
        configureChart("uRem Per Hour", RadiationUnit.MICROREM, "0");
    }//GEN-LAST:event_unitMenuItemMicroRemActionPerformed

    private void unitMenuItemMillisievertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemMillisievertActionPerformed
        configureChart("mSv Per Hour", RadiationUnit.MILLISIEVERT, "0.000");
    }//GEN-LAST:event_unitMenuItemMillisievertActionPerformed

    private void unitMenuItemMicrosievertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemMicrosievertActionPerformed
        configureChart("uSv Per Hour", RadiationUnit.MICROSIEVERT, "0.00");
    }//GEN-LAST:event_unitMenuItemMicrosievertActionPerformed

    private void unitMenuItemCPSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemCPSActionPerformed
        configureChart("Counts Per Second", RadiationUnit.CPS, "0");
    }//GEN-LAST:event_unitMenuItemCPSActionPerformed

    private void unitMenuItemCPMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemCPMActionPerformed
        configureChart("Counts Per Minute", RadiationUnit.CPM, "0");
    }//GEN-LAST:event_unitMenuItemCPMActionPerformed

    private void unitMenuItemMicroRoentgenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemMicroRoentgenActionPerformed
        configureChart("uRoentgen Per Hour", RadiationUnit.MICROROENTGEN, "0");
    }//GEN-LAST:event_unitMenuItemMicroRoentgenActionPerformed

    private void unitMenuItemBananaEquivalentDoseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unitMenuItemBananaEquivalentDoseActionPerformed
        configureChart("Bananas Per Hour", RadiationUnit.BANANA_EQUIVALENT_DOSE, "0");
    }//GEN-LAST:event_unitMenuItemBananaEquivalentDoseActionPerformed

    private void configureChart(String title, RadiationUnit unit, String decimalPattern) {
        chart.setTitle(title);
        radiationUnit = unit;
        chart.getStyler().setYAxisDecimalPattern(decimalPattern);
        reset();
    }

    @Override
    public WindowSpecifications getWindowSpecifications() {
        return new WindowSpecifications(this.windowType, this.getSize(), this.getLocation(), getSettings());
    }

    private Map getSettings() {
        Map settings = new HashMap<String, String>();
        settings.put("units", chart.getTitle());
        settings.put("columns", String.valueOf(maxColumns));
        settings.put("theme", this.theme.toString());
        return settings;
    }

    private void loadSettings(Map<String, String> settings) {
        switch (settings.get("units")) {
            case "mRoentgen Per Hour":
                configureChart("mRoentgen Per Hour", RadiationUnit.MICROROENTGEN, "0.000");
                break;
            case "mRem Per Hour":
                configureChart("mRem Per Hour", RadiationUnit.MILLIREM, "0.00");
                break;
            case "uRem Per Hour":
                configureChart("uRem Per Hour", RadiationUnit.MICROREM, "0");
                break;
            case "mSv Per Hour":
                configureChart("mSv Per Hour", RadiationUnit.MILLISIEVERT, "0.000");
                break;
            case "uSv Per Hour":
                configureChart("uSv Per Hour", RadiationUnit.MICROSIEVERT, "0.00");
                break;
            case "Counts Per Second":
                configureChart("Counts Per Second", RadiationUnit.CPS, "0");
                break;
            case "Counts Per Minute":
                configureChart("Counts Per Minute", RadiationUnit.CPM, "0");
                break;
            case "uRoentgen Per Hour":
                configureChart("uRoentgen Per Hour", RadiationUnit.MICROROENTGEN, "0");
                break;
            case "Bananas Per Hour":
                configureChart("Bananas Per Hour", RadiationUnit.BANANA_EQUIVALENT_DOSE, "0");
                break;
        }
        maxColumns = Integer.valueOf(settings.get("columns"));
        switch (settings.get("theme")) {
            case "DEFAULT":
                setChartColors(Color.WHITE, new Color[]{Color.BLUE}, Color.LIGHT_GRAY, Color.GRAY);
                this.theme = Theme.DEFAULT;
                break;
            case "URANIUM":
                setChartColors(Color.LIGHT_GRAY, new Color[]{Color.DARK_GRAY}, Color.BLACK, Color.BLACK);
                this.theme = Theme.URANIUM;
                break;
            case "CHERENKOV":
                setChartColors(Color.BLUE, new Color[]{Color.CYAN}, Color.DARK_GRAY, Color.BLACK);
                this.theme = Theme.CHERENKOV;
                break;
            case "RADIUM":
                setChartColors(Color.BLACK, new Color[]{Color.GREEN}, Color.DARK_GRAY, Color.GREEN);
                this.theme = Theme.RADIUM;
                break;
            case "FALLOUT":
                setChartColors(Color.BLUE, new Color[]{Color.YELLOW}, Color.DARK_GRAY, Color.WHITE);
                this.theme = Theme.FALLOUT;
                break;
            case "MONO_1":
                setChartColors(Color.WHITE, new Color[]{Color.BLACK}, Color.BLACK, Color.WHITE);
                this.theme = Theme.MONO_1;
                break;
            case "MONO_2":
                setChartColors(Color.BLACK, new Color[]{Color.WHITE}, Color.WHITE, Color.BLACK);
                this.theme = Theme.MONO_2;
                break;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(XChartForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(XChartForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(XChartForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(XChartForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new XChartForm().setVisible(true);
            }
        });
    }

    @Override
    public void updateReading(GMCReading reading) {
        if (isPaused) {
            return;
        }
        if (xData.size() > maxColumns) {
            while (xData.size() > maxColumns) {
                xData.remove(0);
                yData.remove(0);
            }
        }
        switch (radiationUnit) {
            case CPM:
                yData.add((double) reading.getCpm());
                break;
            case CPS:
                yData.add((double) reading.getCps());
                break;
            case MICROSIEVERT:
                yData.add((double) reading.getMicroSievert());
                break;
            case MILLISIEVERT:
                yData.add((double) reading.getMilliSievert());
                break;
            case MICROREM:
                yData.add(reading.getMicroRem());
                break;
            case MILLIREM:
                yData.add((double) reading.getMilliRem());
                break;
            case MICROROENTGEN:
                yData.add((double) reading.getMicroRoentgen());
                break;
            case MILLIROENTGEN:
                yData.add((double) reading.getMilliRoentgen());
                break;
            case BANANA_EQUIVALENT_DOSE:
                int bananaEquivalentDoseEven = (int) reading.getBananaEquivalentDose();
                yData.add((double) bananaEquivalentDoseEven);
                break;
        }
        xData.add(new Date());
        chart.updateXYSeries("series", xData, yData, null);
        panel.repaint();
    }

    @Override
    public void reset() {
        xData.clear();
        yData.clear();
        panel.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu columnsMenu;
    private javax.swing.JMenuItem columnsMenuItem10;
    private javax.swing.JMenuItem columnsMenuItem100;
    private javax.swing.JMenuItem columnsMenuItem1000;
    private javax.swing.JMenuItem columnsMenuItem10000;
    private javax.swing.JMenuItem columnsMenuItem25;
    private javax.swing.JMenuItem columnsMenuItem5;
    private javax.swing.JMenuItem columnsMenuItem500;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenu pauseMenu;
    private javax.swing.JMenuItem pauseMenuItemPause;
    private javax.swing.JMenu themeMenu;
    private javax.swing.JMenuItem themeMenuItemCherenkov;
    private javax.swing.JMenuItem themeMenuItemDefault;
    private javax.swing.JMenuItem themeMenuItemFallout;
    private javax.swing.JMenuItem themeMenuItemMono1;
    private javax.swing.JMenuItem themeMenuItemMono2;
    private javax.swing.JMenuItem themeMenuItemUranium;
    private javax.swing.JMenu unitMenu;
    private javax.swing.JMenuItem unitMenuItemBananaEquivalentDose;
    private javax.swing.JMenuItem unitMenuItemCPM;
    private javax.swing.JMenuItem unitMenuItemCPS;
    private javax.swing.JMenuItem unitMenuItemMicroRem;
    private javax.swing.JMenuItem unitMenuItemMicroRoentgen;
    private javax.swing.JMenuItem unitMenuItemMicrosievert;
    private javax.swing.JMenuItem unitMenuItemMilliRen;
    private javax.swing.JMenuItem unitMenuItemMilliRoentgen;
    private javax.swing.JMenuItem unitMenuItemMillisievert;
    // End of variables declaration//GEN-END:variables
}
