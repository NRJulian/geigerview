/*
GeigerView
Copyright (C) 2021 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.geigerview.model;

public class GMCReading {
    int cpm;
    int cps;
    double microSievert;
    double milliSievert;
    double sievert;
    double bananaEquivalentDose;
    double microRem;
    double milliRem;
    double Rem;
    double microRoentgen;
    double milliRoentgen;
    double roentgen;
    double joulePerKilogram;
    double dentalXRays;
    double mammograms;
    double hoursBackground;
    double daysBackground;  
    
    public GMCReading(int cpm) {
      this.cpm = cpm;
      this.cps = cpm/60;
      this.microSievert = roundToHundredths(this.cpm*0.00625);
      this.milliSievert = roundToHundredths(this.microSievert/1000);
      this.sievert = roundToThousandths(this.milliSievert/1000);
      this.bananaEquivalentDose = roundToHundredths(this.microSievert/0.1);
      this.microRem = roundToHundredths(this.microSievert * 100);
      this.milliRem = roundToHundredths(this.microSievert * 0.1);
      this.Rem = roundToThousandths(this.microSievert * 0.0001);
      this.microRoentgen = roundToHundredths(this.microSievert * 107.19);
      this.milliRoentgen = roundToHundredths(this.microSievert * 0.107185);
      this.roentgen = roundToThousandths(this.microSievert * 0.0001);
      this.joulePerKilogram = roundToHundredths(this.microSievert * 0.000001);
      this.dentalXRays = roundToHundredths(this.microSievert/5);
      this.mammograms = roundToHundredths(this.microSievert/400);
      this.hoursBackground = roundToHundredths(this.microSievert/0.42);
      this.daysBackground = roundToHundredths(this.microSievert/10);
    }
    
    final double roundToHundredths(double number) {
        return Math.round(number * 100.0) / 100.0;
    }

    final double roundToThousandths(double number) {
        return Math.round(number * 1000.0) / 1000.0;
    }
        
    public int getCpm() {
        return cpm;
    }

    public int getCps() {
        return cps;
    }

    public double getMicroSievert() {
        return microSievert;
    }

    public double getMilliSievert() {
        return milliSievert;
    }

    public double getSievert() {
        return sievert;
    }

    public double getBananaEquivalentDose() {
        return bananaEquivalentDose;
    }

    public double getMicroRem() {
        return microRem;
    }

    public double getMilliRem() {
        return milliRem;
    }

    public double getRem() {
        return Rem;
    }

    public double getMicroRoentgen() {
        return microRoentgen;
    }

    public double getMilliRoentgen() {
        return milliRoentgen;
    }

    public double getRoentgen() {
        return roentgen;
    }

    public double getJoulePerKilogram() {
        return joulePerKilogram;
    }

    public double getDentalXRays() {
        return dentalXRays;
    }

    public double getMammograms() {
        return mammograms;
    }

    public double getHoursBackground() {
        return hoursBackground;
    }

    public double getDaysBackground() {
        return daysBackground;
    }    
}