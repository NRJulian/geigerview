/*
GeigerView
Copyright (C) 2021 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.geigerview.model;

import java.awt.Dimension;
import java.awt.Point;
import java.util.Map;

public class WindowSpecifications {
    public enum WindowType {
        GAUGE,
        VISUALIZATION,
        UNITS,
        GRAPH
    }
    WindowType windowType;
    Point location;
    Dimension dimension;
    Map settings;
    
    public WindowSpecifications () {
        //Empty constructor
    }
    
    public WindowSpecifications (WindowType windowType, Dimension dimension, Point location, Map<String, String> settings) {
        this.windowType = windowType;
        this.dimension = dimension;
        this.location = location;    
        this.settings = settings;
    }
    
    public WindowType getWindowType() {
        return windowType;
    }

    public void setWindowType(WindowType windowType) {
        this.windowType = windowType;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }    

    public Map getSettings() {
        return settings;
    }

    public void setSettings(Map settings) {
        this.settings = settings;
    }    
}