/*
GeigerView
Copyright (C) 2021 Nick Julian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.wizarddigital.geigerview.util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

//Adapted from http://java-sl.com/tip_adapt_label_font_size.html
public class AutoResizeLabel extends JLabel {

    public static final int MIN_FONT_SIZE = 3;
    public static final int MAX_FONT_SIZE = 240;
    Graphics graphics;

    public AutoResizeLabel(String text) {
        super(text);
        init();
    }

    protected void init() {
        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                adaptLabelFont(AutoResizeLabel.this);
            }
        });
    }

    protected void adaptLabelFont(JLabel label) {
        if (graphics == null) {
            return;
        }
        Rectangle labelSize = label.getBounds();
        int fontSize = MIN_FONT_SIZE;
        Font font = label.getFont();

        Rectangle smallerFontTextSize = new Rectangle();
        Rectangle largerFontTextSize = new Rectangle();
        while (fontSize < MAX_FONT_SIZE) {
            smallerFontTextSize.setSize(getTextSize(label, font.deriveFont(font.getStyle(), fontSize)));
            largerFontTextSize.setSize(getTextSize(label, font.deriveFont(font.getStyle(), fontSize + 5)));
            if (labelSize.contains(smallerFontTextSize) && !labelSize.contains(largerFontTextSize)) {
                break;
            }
            fontSize = fontSize + 5;
        }

        setFont(font.deriveFont(font.getStyle(), fontSize));
        repaint();
    }

    private Dimension getTextSize(JLabel label, Font font) {
        Dimension size = new Dimension();
        graphics.setFont(font);
        FontMetrics fontMetrics = graphics.getFontMetrics(font);
        //If the label contains multi-line HTML
        if (label.getText().contains("<html>")) {
            ArrayList<String> lines = extractLinesFromHTML(label.getText());
            size.width = fontMetrics.stringWidth(getLongestString(lines));
        } else {
            size.width = fontMetrics.stringWidth(label.getText());
        }
        size.height = fontMetrics.getHeight();
        return size;
    }

    public ArrayList<String> extractLinesFromHTML(String stringWithHTML) {
        String[] lines = stringWithHTML.split("<br>");
        ArrayList<String> strings = new ArrayList<>();
        for (String string : lines) {
            strings.add(string.replace("<html>", "").replace("</html>", ""));
        }
        return strings;
    }

    public static String getLongestString(ArrayList<String> strings) {
        String longestString = strings.get(0);
        for (String string : strings) {
            if (string.length() > longestString.length()) {
                longestString = string;
            }
        }
        return longestString;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        adaptLabelFont(this);
        super.paintComponent(graphics);
        this.graphics = graphics;
    }

}
